import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
    getUser,
    createUser
} from './actions/user';

import {
    getMessages,
    addMessage
} from './actions/messages';

import ChatList from './components/ChatList';
import CreateUser from './components/CreateUser';
import CreateMessage from './components/CreateMessage';

import './index.scss';

class App extends Component {
    componentDidMount() {
        this.props.getUser();
        this.props.getMessages();
    }

    render() {
        return (
            <div>
                {
                    this.props.user ? (
                        <section className="chat">
                            <ChatList user={this.props.user} messages={this.props.messages} />
                            <CreateMessage user={this.props.user} addMessage={this.props.addMessage} />
                        </section>
                    ) : (
                        <CreateUser createUser={this.props.createUser} />
                    )
                }
            </div>
        )
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getUser: bindActionCreators(getUser, dispatch),
        createUser: bindActionCreators(createUser, dispatch),
        getMessages: bindActionCreators(getMessages, dispatch),
        addMessage: bindActionCreators(addMessage, dispatch)
    }
}

export default connect(state => {
    return {
        user: state.user.user,
        messages: state.messages.messages
    }
}, mapDispatchToProps)(App);
