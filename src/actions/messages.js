import {
    ADD_MESSAGE,
    GET_MESSAGES
} from '../constants';

export function getMessages() {
    return dispatch => {
        const messages = JSON.parse(window.localStorage.getItem('messages'));

        if (messages) {
            dispatch({
                type: GET_MESSAGES,
                payload: messages
            });
        }
    };
}

export function addMessage(message) {
    return dispatch => {
        let messages = JSON.parse(window.localStorage.getItem('messages'));

        if (messages && messages.length > 0) {
            window.localStorage.setItem('messages', JSON.stringify([...messages, message]));
        } else {
            window.localStorage.setItem('messages', JSON.stringify([message]));
        }

        dispatch({
            type: ADD_MESSAGE,
            payload: message
        });
    }
}