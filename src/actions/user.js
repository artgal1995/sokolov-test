import {
    SET_USER
} from '../constants';

export function getUser() {
    return dispatch => {
        const user = window.sessionStorage.getItem('user');

        if (user) {
            dispatch({
               type: SET_USER,
               payload: user
            });
        }
    };
}

export function createUser(user) {
    return dispatch => {
        window.sessionStorage.setItem('user', user);

        dispatch({
           type: SET_USER,
           payload: user
        });
    };
}