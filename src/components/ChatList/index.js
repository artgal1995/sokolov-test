import React, { Component } from 'react';
import './index.scss';

export default class ChatList extends Component {
    render() {
        const data = JSON.parse(window.localStorage.getItem('messages'));
        const messages = data && data.length > 0 ? data.map((item) => {
            return (
                <div
                    key={item.id}
                    className={
                        `chat-list__item ${item.author === this.props.user ? 'chat-list__item--right' : null}`
                    }
                >
                    <span className="chat-list__item-author">
                        <b>От кого:</b>
                        {item.author}
                    </span>
                    <span className="chat-list__item-text">
                        {item.text}
                    </span>
                    <span className="chat-list__item-date">
                        {
                            `
                                ${new Date(item.date).toLocaleDateString()}
                                -
                                ${new Date(item.date).toLocaleTimeString()}
                            `
                        }
                    </span>
                </div>
            )
        }) : null;

        return (
            <div className='chat-list'>
                <div className="container">
                    {messages}
                </div>
            </div>
        )
    }
}