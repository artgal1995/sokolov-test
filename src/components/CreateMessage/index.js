import React, { Component } from 'react';
import './index.scss';

export default class CreateMessage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            message: ''
        }
    }

    onMessageChange = (e) => {
        this.setState({message: e.target.value});
    };

    save = (e) => {
        e.preventDefault();
        if (this.state.message.length > 0) {
            const message = {
                id: Math.random().toString(36).substring(2, 8) + Math.random().toString(36).substring(2, 8),
                author: this.props.user,
                text: this.state.message,
                date: new Date()
            };

            this.props.addMessage(message);

            this.setState({
                message: ''
            });
        }

    };

    render() {
        return (
            <section className='create-message'>
                <div className="container">
                    <form onSubmit={this.save} className="create-message__form">
                        <textarea
                            className="create-message__form-textarea"
                            name='message'
                            value={this.state.message}
                            onChange={this.onMessageChange}
                            placeholder='Сообщение'
                        />
                        <button className="create-message__form-button" type='submit'>Отправить</button>
                    </form>
                </div>
            </section>
        )
    };
}