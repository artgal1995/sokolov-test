import React, { Component } from 'react';
import './index.scss';

export default class CreateUser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: ''
        }
    }

    onNameChange = (e) => {
        this.setState({name: e.target.value});
    };

    save = (e) => {
        e.preventDefault();
        this.props.createUser(this.state.name);
    };

    render() {
        return (
            <section className='create-user'>
                <div className="container">
                    <h2 className="create-user__title">Создание пользователя</h2>
                    <form onSubmit={this.save} className="create-user__form">
                        <input
                            className="create-user__form-input"
                            name='user'
                            type="text"
                            value={this.state.user}
                            onChange={this.onNameChange}
                            placeholder='Ваше имя'
                            required
                        />
                        <button className="create-user__form-button" type='submit'>Сохранить</button>
                    </form>
                </div>
            </section>
        )
    };
};