import {
    ADD_MESSAGE,
    GET_MESSAGES
} from '../constants';

const initialState = {
    messages: []
};

export default function(state = initialState, action ) {
    switch (action.type) {
        case GET_MESSAGES: {
            return {
                messages: action.payload,
            }
        }

        case ADD_MESSAGE: {
            return {
                messages: state.messages.length > 0 ? [...state.messages, action.payload] : [action.payload],
            }
        }

        default:
            return state;
    }
}